<?php

namespace Zidget\Models;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;
use Zidget\Events\Menu\MenuWasDeleted;

class Menu extends Model
{
    use NodeTrait;

    protected $fillable = [
        'name', 'group',
        'link', 'type', 'icon',
    ];
}
