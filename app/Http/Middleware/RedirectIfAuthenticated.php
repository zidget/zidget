<?php

namespace Zidget\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null, $route = null)
    {
        if (Auth::guard($guard)->check()) {
            $url = $route ? route($route) : '/';
            return redirect($url);
        }

        return $next($request);
    }
}
