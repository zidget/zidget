<?php

namespace Zidget\Http\Controllers\Admin;

use Zidget\Http\Controllers\Controller;

class AuthController extends Controller
{
    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view_backend('auth.login');
    }

    /**
     * Show the application's register form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegisterForm()
    {
        return view_backend('auth.register');
    }
}
