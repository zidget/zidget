<?php

namespace Zidget\Http\Controllers\User;

use Zidget\Http\Controllers\Controller;

class AuthController extends Controller
{
    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('frontend.user.auth.login');
    }
}
