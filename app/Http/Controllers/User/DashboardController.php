<?php

namespace Zidget\Http\Controllers\User;

use Zidget\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('frontend.user.dashboard');
    }
}
