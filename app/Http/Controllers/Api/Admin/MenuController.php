<?php

namespace Zidget\Http\Controllers\Api\Admin;

use Illuminate\Http\Request;
use Zidget\Http\Controllers\Controller;
use Zidget\Models\Menu;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = Menu::defaultOrder()->get();
        $menus = $menus->map(function($menu) {
            if ($menu->type == 'route') {
                $menu->route = route($menu->link);
            }

            return $menu;
        });

        return $menus->toTree();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $menu = new Menu;
        $menu->fill($request->all());
        $menu->save();
    }
    /**
     * Order menus
     *
     * @param  Request $request [description]
     * @return \Illuminate\Http\Response
     */
    public function order(Request $request, Menu $menu)
    {
        if ($left_id = $request->get('leftId')) {
            $cat_left = Menu::findOrFail($left_id);
            $menu->insertAfterNode($cat_left);
        } elseif ($right_id = $request->get('rightId')) {
            $cat_right = Menu::findOrFail($right_id);
            $menu->insertBeforeNode($cat_right);
        } elseif ($parent_id = $request->get('parentId')) {
            $cat_parent = Menu::findOrFail($parent_id);
            $menu->appendToNode($cat_parent)->save();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Menu $menu)
    {
        $menu->fill($request->all());
        $menu->save();

        return $menu;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menu $menu)
    {
        $menu->delete();
    }
}
