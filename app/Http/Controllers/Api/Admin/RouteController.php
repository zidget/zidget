<?php

namespace Zidget\Http\Controllers\Api\Admin;

use Illuminate\Http\Request;
use Zidget\Http\Controllers\Controller;
use Route;

class RouteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $routes = collect(Route::getRoutes())->map(function ($route) {
            return $route->getName();
        })->filter(function ($route) {
            return $route && explode('.', $route)[0] != 'api';
        })->values();

        return $routes;
    }
}
