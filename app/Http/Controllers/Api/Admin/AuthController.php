<?php

namespace Zidget\Http\Controllers\Api\Admin;

use Zidget\Http\Controllers\Api\AuthController as BaseAuthController;
use Zidget\Http\Requests\Admin\RegisterFormRequest;
use Zidget\Models\Admin;
use Zidget\Models\Role;

class AuthController extends BaseAuthController
{
    protected $guard = 'admin';

    /**
     * Register a new user and get a JWT.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(RegisterFormRequest $request)
    {
        $admin = new Admin;
        $admin->fill($request->all());
        $admin->role_id = Role::whereName('administrator')->firstOrFail()->id;
        $admin->active = true;
        $admin->save();

        return $this->login();
    }
}
