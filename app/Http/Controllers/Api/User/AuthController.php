<?php

namespace Zidget\Http\Controllers\Api\User;

use Zidget\Http\Controllers\Api\AuthController as BaseAuthController;

class AuthController extends BaseAuthController
{
    protected $guard = 'user';
}
