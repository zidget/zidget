<?php

if (!function_exists('elixir_frontend')) {
    /**
     * Get the path to a versioned Elixir frontend file.
     *
     * @param string $file
     * @param string $buildDirectory
     *
     * @throws \InvalidArgumentException
     *
     * @return string
     */
    function elixir_frontend($file, $buildDirectory = 'build')
    {
        $file = 'frontend/' . $file;

        return elixir($file, $buildDirectory);
    }
}

if (!function_exists('elixir_backend')) {
    /**
     * Get the path to a versioned Elixir backend file.
     *
     * @param string $file
     * @param string $buildDirectory
     *
     * @throws \InvalidArgumentException
     *
     * @return string
     */
    function elixir_backend($file, $buildDirectory = 'build')
    {
        $file = 'backend/' . $file;

        return elixir($file, $buildDirectory);
    }
}

if (!function_exists('make_array')) {
    /**
     * Make array.
     *
     * @param string/array $data
     *
     * @return array
     */
    function make_array($data)
    {
        return is_array($data) ? $data : [$data];
    }
}

if (!function_exists('view_widget')) {
    /**
     * Get the evaluated view widget contents for the given view.
     *
     * @param string $view
     * @param array  $data
     * @param array  $mergeData
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    function view_widget($view = null, $data = [], $mergeData = [])
    {
        return view($view, $data, $mergeData);
    }
}

if (!function_exists('view_module')) {
    /**
     * Get the evaluated view module contents for the given view.
     *
     * @param string $view
     * @param array  $data
     * @param array  $mergeData
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    function view_module($view = null, $data = [], $mergeData = [])
    {
        $view = $view ? 'module_' . $view : $view;

        return view($view, $data, $mergeData);
    }
}

if (!function_exists('view_backend')) {
    /**
     * Get the evaluated view backend contents for the given view.
     *
     * @param string $view
     * @param array  $data
     * @param array  $mergeData
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    function view_backend($view = null, $data = [], $mergeData = [])
    {
        $view = $view ? 'backend.' . $view : $view;

        return view($view, $data, $mergeData);
    }
}

if (!function_exists('public_asset')) {
    /**
     * Generate an asset path for the application.
     *
     * @param string $path
     * @param bool   $secure
     *
     * @return string
     */
    function public_asset($path, $secure = null)
    {
        $path = starts_with($path, '/') ? $path : '/' . $path;

        return app('url')->asset('/public' . $path, $secure);
    }
}

if (!function_exists('rchmod')) {
    /**
     * Recursive chmod.
     *
     * @param string $path
     * @param int    $filePerm
     * @param int    $dirPerm
     *
     * @return bool
     */
    function rchmod($path, $filePerm = 0644, $dirPerm = 0755)
    {
        if (!file_exists($path)) {
            return false;
        }

        if (is_file($path)) {
            chmod($path, $filePerm);
        } elseif (is_dir($path)) {
            $foldersAndFiles = scandir($path);

            $entries = array_slice($foldersAndFiles, 2);

            foreach ($entries as $entry) {
                rchmod($path . DIRECTORY_SEPARATOR . $entry, $filePerm, $dirPerm);
            }
            chmod($path, $dirPerm);
        }

        return true;
    }
}

if (!function_exists('env_replace')) {
    /**
     * Replace an environement content.
     *
     * @param string $keyToRepace
     * @param string $value
     *
     * @return string
     */
    function env_replace($keyToRepace, $value)
    {
        $keyExist = false;
        $value    = str_contains($value, ' ') ? '"' . $value . '"' : $value;

        $envPath    = base_path('.env');
        $envContent = file($envPath);

        $newContent = array_map(function ($content) use ($keyToRepace, &$keyExist, $value) {
            $element = explode('=', $content);
            $key     = $element[0];

            if ($keyToRepace == $key) {
                $keyExist = true;

                return $key . '=' . $value . "\n";
            } else {
                return $content;
            }
        }, $envContent);

        if (!$keyExist) {
            array_push($newContent, $keyToRepace . '=' . $value . "\n");
        }

        file_put_contents($envPath, implode('', $newContent));
        putenv($keyToRepace . '=' . $value);
    }
}

if (!function_exists('is_numeric_array')) {
    /**
     * Check whether an array is composed only with numeric values or not.
     *
     * @param array $array
     *
     * @return bool
     */
    function is_numeric_array($array)
    {
        foreach ($array as $element) {
            if (!is_numeric($element)) {
                return false;
            }
        }

        return true;
    }
}

if (!function_exists('image_route')) {
    /**
     * Get image route.
     *
     * @param string $type
     * @param string $image
     *
     * @return string
     */
    function image_route($type, $image)
    {
        if ($image) {
            $image = '/' . $image;
        }

        return public_asset(config("zidget.images.{$type}.path") . $image);
    }
}

if (!function_exists('in_arrayi')) {
    /**
     * Insensitive in_array.
     *
     * @param string $needle
     * @param array  $haystack
     *
     * @return bool
     */
    function in_arrayi($needle, $haystack)
    {
        return in_array(strtolower($needle), array_map('strtolower', $haystack));
    }
}

if (!function_exists('string_between')) {
    /**
     * Get string between two strings.
     *
     * @param string $string
     * @param string $start
     * @param string $end
     *
     * @return string
     */
    function string_between($string, $start, $end)
    {
        $string = ' ' . $string;
        $ini    = strpos($string, $start);
        if ($ini == 0) {
            return '';
        }

        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;

        return substr($string, $ini, $len);
    }
}
