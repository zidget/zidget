<?php

namespace Zidget\Exceptions;

use Exception;

class WidgetNotFoundException extends Exception
{
}
