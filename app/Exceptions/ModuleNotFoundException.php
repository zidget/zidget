<?php

namespace Zidget\Exceptions;

use Exception;

class ModuleNotFoundException extends Exception
{
}
