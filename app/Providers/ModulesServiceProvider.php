<?php

namespace Zidget\Providers;

use Illuminate\Support\ServiceProvider;
use Modules;
use Zidget\Repositories\ModulesRepository;
use Zidget\Support\Stub;

class ModulesServiceProvider extends ServiceProvider
{
    /**
     * Modules.
     *
     * @var Modules
     */
    protected $modules;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Modules::bootAll();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        Stub::setBasePath(config('modules.stubs.path'));

        $this->app->singleton('modules', function () {
            $modules = new ModulesRepository(config('modules.paths.modules'));
            $modules->registerAll();

            return $modules;
        });
    }
}
