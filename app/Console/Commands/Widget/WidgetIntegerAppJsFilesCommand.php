<?php

namespace Zidget\Console\Commands\Widget;

use File;
use Illuminate\Console\Command;
use Widgets;

class WidgetIntegerAppJsFilesCommand extends Command
{
    /**
     * Widget Author.
     */
    protected $author;

    /**
     * Widget name.
     */
    protected $name;

    /**
     * Widget type.
     */
    protected $type;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'widget:integer-appjs-files';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Integer widgets app.js files to global app.js asset.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Integrating app.js files...');
        if ($this->updateAppJsFile()) {
            $this->info('Components synchronized.');
        }else{
            $this->error('Nothing to publish.');
        }
    }

    /**
     * Update app js file
     */
    protected function updateAppJsFile()
    {
        $file = base_path("resources/assets/js/app.js");
        $content = file_get_contents($file);
        $startTag = "\/\/ <widgets>\n\n";
        $endTag = "\n\n\/\/ <\/widgets>";
        $regex = "/".$startTag."([\s\S]*)".$endTag."/msU";
        preg_match($regex, $content, $matches);

        if (!isset($matches[1])) {
            return false;
        }

        $appFiles = $this->getAllWidgetsAppJsFiles();

        $requireAppFiles = implode("\n", $appFiles);
        $content = str_replace($matches[0], stripslashes($startTag).$requireAppFiles.stripslashes($endTag), $content);

        file_put_contents($file, $content);

        return true;
    }

    protected function getAllWidgetsAppJsFiles()
    {
        $widgets = \Widgets::noType()->noFilter()->all();
        $app = [];

        foreach ($widgets as $name => $widget) {
            $app[] = "require('./apps/widgets/" . str_replace('\\', '/', strtolower($name)) . "/app.js')";
            $this->info("[ + ] Integrating $name files");
        }

        return $app;
    }
}
