<?php

namespace Zidget\Console\Commands\Widget;

use File;
use Illuminate\Console\Command;
use Widgets;

class WidgetPublishComponentsCommand extends Command
{
    /**
     * Widget Author.
     */
    protected $author;

    /**
     * Widget name.
     */
    protected $name;

    /**
     * Widget type.
     */
    protected $type;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'widget:publish-components
                            {type? : Widget type}
                            {author? : Widget author}
                            {name? : Widget name}
                            {--force : force publishing widget assets}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish widget assets.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->author = $this->argument('author');
        $this->name = $this->argument('name');
        $this->type = $this->argument('type');

        if (!$this->author && !$this->name && !$this->type) {
            $this->publishAll();

            return;
        }

        if (!$this->validWidgetType()) {
            $this->error('Invalid Widget Type, please choose between [Frontend, Backend]');

            return;
        }

        $this->publish();
    }

    protected function publishAll()
    {
        foreach (Widgets::noType()->noFilter()->all() as $widget) {
            $this->call('widget:publish-components', [
                'author'  => $widget->author,
                'name'    => $widget->name,
                'type'    => $widget->type,
                '--force' => $this->option('force'),
            ]);
        }
    }

    protected function validWidgetType()
    {
        return in_array($this->type, ['Frontend', 'Backend']);
    }

    protected function publish()
    {
        $this->output->writeln('');
        $this->table([], [["Publishing Components of Widget <info>{$this->type}/{$this->author}/{$this->name}</info>"]]);
        $this->output->writeln('');

        $widgetPath = $this->getWidgetPath();
        $widgetComponentsPath = $this->getWidgetComponentsPath();
        $widgetAppJsPath = $this->getWidgetAppJsPath();

        if (!File::isDirectory($widgetPath)) {
            $this->error("Widget [{$this->type}/{$this->author}/{$this->name}] doesn't exist!");
            return;
        }

        if (!File::isDirectory($widgetPath.'/assets/src/js/components')) {
            $this->info("[ ~ ] Nothing to publish for Widget [{$this->type}/{$this->author}/{$this->name}]");

            return;
        }

        if (!File::exists($widgetPath.'/assets/src/js/app.js')) {
            $this->info("[ ~ ] Nothing to publish for Widget [{$this->type}/{$this->author}/{$this->name}]");

            return;
        }

        if (File::isDirectory($widgetComponentsPath) || File::isDirectory($widgetAppJsPath)) {
            if ($this->option('force')) {
                @unlink($widgetComponentsPath);
                @unlink($widgetAppJsPath);
            } else {
                $this->error("Components or Apps of Widget [{$this->type}/{$this->author}/{$this->name}] already exist!");

                return;
            }
        }

        $this->info("[ + ] Creating $widgetComponentsPath");

        if (!File::exists(dirname($widgetComponentsPath))) {
            File::makeDirectory(dirname($widgetComponentsPath), 0775, true);
        }

        $this->info("[ + ] Creating $widgetAppJsPath");

        if (!File::exists($widgetAppJsPath)) {
            File::makeDirectory($widgetAppJsPath, 0775, true);
        }

        symlink($widgetPath.'/assets/src/js/components', $widgetComponentsPath);
        symlink($widgetPath.'/assets/src/js/app.js', $widgetAppJsPath.'/app.js');

        $this->info('Components published');
    }

    /**
     * Get widget path.
     */
    protected function getWidgetPath()
    {
        return base_path()
        .'/widgets'
        .'/'.$this->type
        .'/'.studly_case($this->author)
        .'/'.studly_case($this->name);
    }

    /**
     * Get widget public assets path.
     */
    protected function getWidgetComponentsPath()
    {
        return base_path('resources/assets/js/components')
        .'/widgets'
        .'/'.strtolower($this->type)
        .'/'.strtolower($this->author)
        .'/'.strtolower($this->name);
    }

    /**
     * Get widget public app js assets path.
     */
    protected function getWidgetAppJsPath()
    {
        return base_path('resources/assets/js/apps')
        .'/widgets'
        .'/'.strtolower($this->type)
        .'/'.strtolower($this->author)
        .'/'.strtolower($this->name);
    }
}
