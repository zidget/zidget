<?php

namespace Zidget\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        /* Widget Commands */
        Commands\Widget\WidgetMakeCommand::class,
        Commands\Widget\WidgetPublishCommand::class,
        Commands\Widget\WidgetArchiveCommand::class,
        Commands\Widget\WidgetPublishComponentsCommand::class,
        Commands\Widget\WidgetIntegerAppJsFilesCommand::class,

        /* Module Commands */
        Commands\Module\ModuleArchiveCommand::class,
        Commands\Module\ModuleMakeCommand::class,
        Commands\Module\ModuleMakeControllerCommand::class,
        Commands\Module\ModuleMakeBackendControllerCommand::class,
        Commands\Module\ModuleMakeProviderCommand::class,
        Commands\Module\ModuleMakeSeedCommand::class,
        Commands\Module\ModuleMakeRequestCommand::class,
        Commands\Module\ModuleMakeWidgetCommand::class,
        Commands\Module\ModuleSeedCommand::class,
        Commands\Module\ModuleMigrateCommand::class,
        Commands\Module\ModuleMigrateResetCommand::class,
        Commands\Module\ModuleMigrateRollbackCommand::class,
        Commands\Module\ModuleMigrateRefreshCommand::class,
        Commands\Module\ModuleListCommand::class,
        Commands\Module\ModuleDisableCommand::class,
        Commands\Module\ModuleEnableCommand::class,
        Commands\Module\ModuleMakeMiddlewareCommand::class,
        Commands\Module\ModuleRouteProviderCommand::class,
        Commands\Module\ModuleMakeModelCommand::class,
        Commands\Module\ModuleMakeConsoleCommand::class,
        Commands\Module\ModulePublishCommand::class,
        Commands\Module\ModulePublishWidgetCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
