<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Widget Namespace
    |--------------------------------------------------------------------------
    |
    | Default widget namespace.
    |
     */

    'namespace' => 'Zidget\Widgets',

    /*
    |--------------------------------------------------------------------------
    | Widget Path
    |--------------------------------------------------------------------------
    |
    | Default widget path.
    |
     */

    'path'      => base_path('widgets'),

    /*
    |--------------------------------------------------------------------------
    | Widget Stubs
    |--------------------------------------------------------------------------
    |
    | Default widget stubs.
    |
     */
    'stubs'     => [
        'path'         => app_path('Console/Commands/Widget/stubs'),
        'folders'      => ['views', 'assets'],
        'files'        => [
            'json'                             => 'zidget.json',
            'views/index'                      => 'views/index.blade.php',
            'views/setting'                    => 'views/setting.blade.php',
            'widget'                           => 'Widget.php',
            'assets/src/js/app'                => 'assets/src/js/app.js',
            'assets/src/js/components/Example' => 'assets/src/js/components/Example.vue',
        ],
        'replacements' => [
            'assets/src/js/app' => [
                'LOWER_TYPE',
                'LOWER_AUTHOR',
                'LOWER_NAME',
            ],
            'views/index' => [
                'STUDLY_NAME',
                'LOWER_TYPE',
                'LOWER_AUTHOR',
                'LOWER_NAME',
            ],
            'json'        => [
                'STUDLY_TYPE',
                'LOWER_TYPE',
                'STUDLY_NAME',
                'STUDLY_AUTHOR',
                'LOWER_NAME',
            ],
            'widget'      => [
                'LOWER_TYPE',
                'LOWER_AUTHOR',
                'LOWER_NAME',
                'STUDLY_TYPE',
                'STUDLY_AUTHOR',
                'STUDLY_NAME',
            ],
        ],
    ],
    'paths'     => [
        /*
        |--------------------------------------------------------------------------
        | Widgets path
        |--------------------------------------------------------------------------
        |
        | This path used for save the generated widget.
        |
         */

        'widgets' => base_path('widgets'),
        /*
        |--------------------------------------------------------------------------
        | Widgets assets path
        |--------------------------------------------------------------------------
        |
         */

        'assets'  => public_path('widgets'),
    ],
];
