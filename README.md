## How to Install

- `composer install`
- `npm install`
- `php artisan jwt:secret`
- edit your .env (to add MYSQL details)
- refresh your database with seeds `php artisan migrate:refresh --seed`
