<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::group(['prefix' => 'auth'], function () {
    Route::group(['middleware' => 'guest:user'], function () {
        Route::post('login', 'AuthController@login')->name('api.user.login');
    });

    Route::group(['middleware' => 'auth:user'], function () {
        Route::post('logout', 'AuthController@logout')->name('api.user.logout');
        Route::post('refresh', 'AuthController@refresh')->name('api.user.refresh');
        Route::post('me', 'AuthController@me')->name('api.user.me');
    });
});
