<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

 Route::name('api.admin.')->prefix('auth')->group(function () {
    Route::group(['middleware' => 'guest:admin'], function () {
        Route::post('login', 'AuthController@login')->name('login');
        Route::post('register', 'AuthController@register')->name('register');
    });

    Route::group(['middleware' => 'auth:admin'], function () {
        Route::post('logout', 'AuthController@logout')->name('logout');
        Route::post('refresh', 'AuthController@refresh')->name('refresh');
        Route::post('me', 'AuthController@me')->name('me');
    });
});

Route::name('api.admin.')->middleware('auth:admin')->group(function () {
    Route::get('route', 'RouteController@index')->name('route.index');
    Route::name('menu.')->prefix('menu')->group(function() {
        Route::get('/', 'MenuController@index')->name('index');
        Route::post('/', 'MenuController@store')->name('store');
        Route::prefix('{menu}')->group(function() {
            Route::put('order', 'MenuController@order')->name('order');
            Route::put('/', 'MenuController@update')->name('update');
            Route::delete('/', 'MenuController@destroy')->name('destroy');
        });
    });
});
