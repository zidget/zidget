<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::group(['prefix' => 'auth', 'middleware' => 'guest:user,user.dashboard.index'], function () {
    Route::get('login', 'AuthController@showLoginForm')->name('user.login');
});

Route::group(['middleware' => 'auth:user'], function () {
    /* Home : /user */
    Route::get('/', 'DashboardController@index')->name('user.dashboard.index');
});
