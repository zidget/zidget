<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

/* /console/auth */
Route::name('admin.')->prefix('auth')->middleware('guest:admin')->group(function () {
    Route::get('login', 'AuthController@showLoginForm')->name('login');
    Route::get('register', 'AuthController@showRegisterForm')->name('register');
});

Route::name('admin.')->middleware('auth:admin')->group(function () {
    /* Home : /console */
    Route::get('/', 'DashboardController@index')->name('dashboard.index');
    /* Menu : /console/menu */
    Route::get('menu', 'MenuController@index')->name('menu.index');
});
