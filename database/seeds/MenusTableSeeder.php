<?php

use Illuminate\Database\Seeder;
use Zidget\Models\Menu;

class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = new Menu;

        $menu->name  = "Dashboard";
        $menu->group = "admin";
        $menu->icon  = "dashboard";
        $menu->link  = "admin.dashboard.index";
        $menu->type  = "route";

        $menu->save();
    }
}
