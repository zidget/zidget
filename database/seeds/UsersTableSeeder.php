<?php

use Illuminate\Database\Seeder;
use Zidget\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::whereName('user')->first();
        $role->users()->create([
            'name'     => 'User',
            'email'    => 'user@example.com',
            'password' => 'password',
        ]);
    }
}
