let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// Admin
mix.js('resources/assets/js/app.js', 'public/admin/js/app.js');
mix.scripts([
    'bower_components/adminbsb-materialdesign/plugins/jquery/jquery.min.js',
    'bower_components/adminbsb-materialdesign/plugins/bootstrap/js/bootstrap.min.js',
    'bower_components/adminbsb-materialdesign/plugins/bootstrap-select/js/bootstrap-select.min.js',
    'bower_components/adminbsb-materialdesign/plugins/jquery-slimscroll/jquery.slimscroll.js',
    'bower_components/adminbsb-materialdesign/plugins/node-waves/waves.min.js',
    'bower_components/adminbsb-materialdesign/plugins/waitme/waitMe.js',
    'bower_components/adminbsb-materialdesign/plugins/momentjs/moment.js',
    'resources/assets/js/libs/nestable/jquery.nestable.js',
    'resources/assets/js/admin.js',
    'resources/assets/js/config.js',
    'public/admin/js/app.js',
], 'public/admin/js/app.js');//.version();


mix.sass('resources/assets/sass/custom.scss', 'public/admin/css/app.css');
mix.styles([
    'bower_components/adminbsb-materialdesign/plugins/bootstrap/css/bootstrap.min.css',
    // 'bower_components/adminbsb-materialdesign/plugins/font-awesome/css/font-awesome.min.css',
    'bower_components/adminbsb-materialdesign/plugins/bootstrap-select/css/bootstrap-select.min.css',
    'bower_components/adminbsb-materialdesign/plugins/node-waves/waves.min.css',
    'bower_components/adminbsb-materialdesign/plugins/animate-css/animate.min.css',
    'bower_components/adminbsb-materialdesign/plugins/waitme/waitMe.css',
    'resources/assets/js/libs/nestable/nestable.css',
    'resources/assets/sass/materialize.css',
    'resources/assets/sass/style.css',
    'bower_components/adminbsb-materialdesign/css/themes/all-themes.min.css',
    'public/admin/css/app.css',
], 'public/admin/css/app.css');//.version();


mix.copy([
   'bower_components/adminbsb-materialdesign/plugins/bootstrap/fonts',
   ], 'public/admin/fonts');


mix.copy([
   'resources/assets/images',
   ], 'public/admin/images');
