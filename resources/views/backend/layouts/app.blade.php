<!DOCTYPE html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="{{ app()->getLocale() }}"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="{{ app()->getLocale() }}"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ config('app.name', 'Zidget') }}</title>

  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta content="{{ csrf_token() }}" name="csrf-token">
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

  <link rel="stylesheet" href="{{ url('admin/css/app.css') }}" media="all">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  @stack('styles')
</head>
<body class="theme-indigo">
    <div id="app">
      <!-- Top Bar -->
      <nav class="navbar">
          <div class="container-fluid">
              <div class="navbar-header">
                  <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                  <a href="javascript:void(0);" class="bars"></a>
                  <a class="navbar-brand" href="{{ route('admin.dashboard.index') }}">ZIDGET</a>
              </div>
              <div class="collapse navbar-collapse" id="navbar-collapse">
                  <backend-navbar logout-url="{{ route('api.admin.logout') }}"></backend-navbar>
              </div>
          </div>
      </nav>
      <!-- #Top Bar -->
      <section>
          <!-- Left Sidebar -->
          <aside id="leftsidebar" class="sidebar">

              <!-- User Info -->
              <div class="user-info">
                  <div class="media">
                      <div class="media-left">
                          <a href="javascript:void(0);">
                            <img src="{{ url('admin/images/user.svg') }}" class="img-circle" width="48" height="48" alt="User" />
                          </a>
                      </div>
                      <div class="media-body info-container">
                          <h4 class="media-heading">{{ auth()->user()->name }}</h4>
                          <div>{{ ucfirst(auth()->user()->role->name) }}</div>
                      </div>
                  </div>
              </div>

              <!-- #User Info -->
              <!-- Menu -->
              <backend-navmenu></backend-navmenu>
              <!-- #Menu -->
              <!-- Footer -->
              <div class="legal">
                  <div class="copyright">
                      &copy; {{ date('Y') }} <a href="javascript:void(0);">Zidget</a>.
                  </div>
                  <div class="version">
                      <b>Version: </b> 1.0.0
                  </div>
              </div>
              <!-- #Footer -->
          </aside>
          <!-- #END# Left Sidebar -->
      </section>

      <section class="content">
          <div class="container-fluid">
            @yield('content')
          </div>
      </section>
    </div>

  <script src="{{ url('admin/js/app.js') }}"></script>
  @stack('scripts')
</body>
</html>
