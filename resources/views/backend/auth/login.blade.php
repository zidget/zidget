@extends('backend::layouts.auth')

@section('content')
<backend-auth-login
    action="{{ route('api.admin.login') }}"
    password-url=""
    register-url="{{ route('admin.register') }}"
></backend-auth-login>
@endsection
