@extends('backend::layouts.auth')

@section('content')
<backend-auth-register
    action="{{ route('api.admin.register') }}"
    login-url="{{ route('admin.login') }}"
></backend-auth-register>
@endsection
