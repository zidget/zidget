@extends('frontend::layouts.auth')

@section('content')
<frontend-login
    action="{{ route('api.user.login') }}"
    password-url=""
></frontend-login>
@endsection
