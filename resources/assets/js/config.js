$(function () {
    $('#nestableList').nestable({
        dropCallback: function(details) {
            var rightId = details.sourceEl.next().data("id"),
                leftId = details.sourceEl.prev().data("id"),
                parentId = details.destId,
                id = details.sourceId;

            if (id) {
                var url = $('#nestableList').data('url') + '/' + id + '/order';

                axios.put(url, {
                    parentId: parentId,
                    rightId: rightId,
                    leftId: leftId
                });
            }
        }
    });

    $('.navbar-right .dropdown-menu .body .menu').slimscroll({
        height: '254px',
        color: 'rgba(0,0,0,0.5)',
        size: '4px',
        alwaysVisible: false,
        borderRadius: '0',
        railBorderRadius: '0'
    });
});
