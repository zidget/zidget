
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('babel-polyfill');

window.Vue = require('vue');

Vue.use(require('vue-cookie'));
Vue.use(require('vuex'));
Vue.use(require('vue-swal'));

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('backend-auth-login', require('./components/backend/auth/login.vue'));
Vue.component('backend-auth-register', require('./components/backend/auth/register.vue'));
Vue.component('backend-navbar', require('./components/backend/navbar.vue'));
Vue.component('backend-navmenu', require('./components/backend/navmenu.vue'));
Vue.component('backend-navsubmenu', require('./components/backend/navsubmenu.vue'));

/* Menu */
Vue.component('backend-menu', require('./components/backend/menu/index.vue'));
Vue.component('backend-menu-list', require('./components/backend/menu/list.vue'));
Vue.component('backend-menu-link', require('./components/backend/menu/link.vue'));
Vue.component('backend-menu-route', require('./components/backend/menu/route.vue'));
Vue.component('backend-menu-nested', require('./components/backend/menu/nested.vue'));
Vue.component('backend-menu-edit', require('./components/backend/menu/edit.vue'));

// <widgets>



// </widgets>
Vue.prototype.$bus = new Vue();

const app = new Vue({
    el: '#app'
});
